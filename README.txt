uc_order again 

A module I knocked out real quick for a site.  It's ugly still.  There are no configs or anything like that.  You have to edit the module if you want to change layout etc.  Sorry about that.  Maybe i'll improve it later on.  I'd like to learn how to formally contribute a module to drupal so when I get some time maybe i'll look into how that works and make it complient etc, but it's pretty basic 

prints out the last 10 orders in a table.  You click on the order and it prints out those products again with a buy button in a 3 wide table grid.  

INSTALL 

Unzip the module in your modules directory (sites/all/modules).  Enable the module and then access it through /utils/orderagain 
(so http://somesite.com/utils/orderagain)

If you want to change the path edit uc_orderagain.module 
  $items['utils/orderagain'] = array( 

line to whatever you want.

To change the grid width 

look for $cols=3; and change that to whatever you want

thanks

-Zachary z@jeffnet.org
